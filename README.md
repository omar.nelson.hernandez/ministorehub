# ministorehub

## Google requirements

### APIs

- IAM
- Bigquery
- Secret manager
- Google Maps

### Configurations

- Create a secret with the Google Maps API key
- Create service accounts for the cloud function
  - BQ permissions: data viewer, job user
  - Secret manager: accessor
- Upload and edit cloud function
  - Edit timeout to 540
  - Add the service account to the cloud function
  - Add env variables
  - Add the secret to the cloud function

"""Driver code."""
import functions_framework
import ministorehub_be.logging
from ministorehub_be.gmaps import GMapsDMController
from ministorehub_be.requests import RequestParser, ResponseGenerator
from ministorehub_be.exceptions import ParameterMissing, StrategyError


@functions_framework.http
def ministorehub_be(request):
    """Process request from webserver.

    Create an OutputAggregator and let it work

    Args:
        request: data from the webserver
    """
    try:
        controller = GMapsDMController()
        controller.set_request_parser(RequestParser(request))
        controller.configure_request()
        controller.make_request()

        return controller.to_json()

    except (ParameterMissing, StrategyError) as exc:
        rsp = ResponseGenerator()
        rsp.set_status("error")
        rsp.set_msg(str(exc))

        return rsp.to_json()

"""Handles data related to travel information."""
import json
from typing import Optional
import logging

logger = logging.getLogger(__name__)


class Coords():
    """Stores latitude and longitude.

    Attributes:
        coord_id: optional id related to the coordinates
        lat: latitude
        lon: longitude
        extra_data: metadata for the coordinate
    """

    def __init__(self, lat: str, lon: str, coord_id: str = "Unknown"):
        """Initialize object."""
        self.coord_id: str = coord_id
        self.lat: str = str(lat)
        self.lon: str = str(lon)
        self.extra_data: dict[str, str] = {}

    def __repr__(self):
        """Return string representation."""
        out = (
            f"Coords("
            f"coord_id={self.coord_id}"
            f",lat={self.lat}"
            f",lon={self.lon}"
            f")"
        )

        return out

    def get_coords(self) -> str:
        """Get coordinate in API compatible format."""
        return ','.join((self.lat, self.lon))

    def set_extra_data(self, data: dict[str, str]):
        """Set extra data for this coordinate pair."""
        self.extra_data = data


class CoordsPair():
    """Stores a pair of coordinates and related data.

    Attributes:
        pt1: first point
        pt2: second point
        distance: distance from pt1 to pt2
        time: time it takes to go from pt1 to pt2
        mode: navigation mode to go from pt1 to pt2
    """

    def __init__(self, pt1: Coords, pt2: Coords):
        """Initialize object."""
        self.pt1: Coords = pt1
        self.pt2: Coords = pt2
        self.distance: Optional[int] = None
        self.time: Optional[int] = None
        self.mode: Optional[str] = None

    def set_distance(self, distance: int):
        """Set distance from pt1 to pt2."""
        self.distance = distance

    def set_time(self, time: int):
        """Set time from pt1 to pt2."""
        self.time = time

    def set_mode(self, mode: str):
        """Set navigation mode between pt1 and pt2."""
        self.mode = mode

    def get_pt1(self) -> str:
        """Get coordinate in API compatible format."""
        return self.pt1.get_coords()

    def get_pt2(self) -> str:
        """Get coordinate in API compatible format."""
        return self.pt2.get_coords()

    def __repr__(self) -> str:
        """Return string representation."""
        out = (
            f"CoordsPair("
            f"pt1={self.pt1}"
            f",pt2={self.pt2}"
            f",distance={self.distance}"
            f",time={self.time}"
            f",mode={self.mode}"
            f")"
        )

        return out

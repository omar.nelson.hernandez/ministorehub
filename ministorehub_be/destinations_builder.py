"""Collection of strategies to calculate destinations."""

import logging
import abc
from ministorehub_be.requests import RequestParser
from ministorehub_be.exceptions import StrategyError

logger = logging.getLogger(__name__)


class DestinationsStrategy(abc.ABC):
    """Base class/interface for all destination strategies."""

    @abc.abstractmethod
    def build_query(self) -> str:
        """Build a query to fetch a list of destinations."""
        raise NotImplementedError


class DestinationsByRadiusStrategy(DestinationsStrategy):
    """Creates a list of destinations based on a radius.

    Attributes:
        _reqp: RequestParser to get request parameters
    """

    def __init__(self, reqp: RequestParser):
        """Initialize object."""
        self._reqp = reqp

    def build_query(self):
        """Build query for destination list by radius.

        Build and return a query string that returns a list of destinations
        taken from an origin and a radius.

        One degree in latitude or longitude = 111,111m
        """
        offset: float = int(self._reqp.get_radius()) / 111111

        px1: float = float(self._reqp.get_origin().lon) - offset
        px2: float = float(self._reqp.get_origin().lon) + offset
        py1: float = float(self._reqp.get_origin().lat) - offset
        py2: float = float(self._reqp.get_origin().lat) + offset

        table: str = self._reqp.get_table()
        id_col: str = self._reqp.get_id_col()
        lat_col: str = self._reqp.get_lat_col()
        lon_col: str = self._reqp.get_lon_col()
        extra_cols: str = self._reqp.get_extra_cols()
        cols: str = ','.join((id_col, lat_col, lon_col, extra_cols))

        lat_bbox_filter: str = (
            lat_col + " BETWEEN " + str(py1) + " AND " + str(py2) + " "
            )

        lon_bbox_filter: str = (
            lon_col + " BETWEEN " + str(px1) + " AND " + str(px2) + " "
            )

        query: str = (
            "SELECT " + cols + " "
            "FROM " + table + " "
            "WHERE " + lat_bbox_filter + " "
            "AND " + lon_bbox_filter + " "
        )

        return query


class DestinationsByIdStrategy(DestinationsStrategy):
    """Creates a list of destinations based on a list of IDs.

    Attributes:
        _reqp: RequestParser to get request parameters
    """

    def __init__(self, reqp: RequestParser):
        """Initialize object."""
        self._reqp = reqp

    def build_query(self):
        """Build query for destination list by a list of IDs.

        Build and return a query string that returns a list of destinations
        taken from a list of IDs.
        """
        table: str = self._reqp.get_table()
        id_col: str = self._reqp.get_id_col()
        lat_col: str = self._reqp.get_lat_col()
        lon_col: str = self._reqp.get_lon_col()
        extra_cols: str = self._reqp.get_extra_cols()
        cols: str = ','.join((id_col, lat_col, lon_col, extra_cols))

        query: str = (
            "SELECT " + cols + " "
            "FROM " + table + " "
            "WHERE " + id_col + " IN (" + self._reqp.get_destinations() + ")"
        )

        return query


def create(reqp: RequestParser):
    """Create a DestinationBuilder appropriate for the parameters."""
    radius: int = reqp.get_radius()
    destinations: str = reqp.get_destinations()
    logger.debug("Radius: [%s] Destinations: [%s]", radius, destinations)
    if radius and not destinations:
        return DestinationsByRadiusStrategy(reqp)
    if not radius and destinations:
        return DestinationsByIdStrategy(reqp)
    raise StrategyError(radius, destinations)

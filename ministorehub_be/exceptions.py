"""Contains all exception code."""


class ParameterMissing(Exception):
    """Exception to throw when an expected parameter is missing.

    Attributes:
        where: expected location of the missing parameter
        param: name of the parameter
        message: message to print
    """

    def __init__(self, where, param, message="Missing parameter"):
        """Exception for missing parameters.

        Raised when a URL or environment parameter are missing

        Args:
            where: expected location of the missing parameter
            param: name of the parameter
            message: message to print
        """
        self.param = param
        self.where = where
        self.message = message
        super().__init__(message)

    def __repr__(self) -> str:
        """Return string representation."""
        out = (
            f"Missing param [{self.param}] from [{self.where}]"
        )
        return out

    def __str__(self) -> str:
        """Return string representation."""
        return self.__repr__()


class StrategyError(Exception):
    """Exception to throw when a strategy cannot be selected.

    Attributes:
        radius: if not empty, selects radius strategy
        destinations: if not empty, selects IDs strategy
    """

    def __init__(self, radius: int, destinations: str):
        """Exception for strategy malfunction.

        Raised when a strategy cannot be selected due to conflicting parameters
        """
        self.radius: int = radius
        self.destinations: str = destinations
        super().__init__("Cannot select strategy")

    def __repr__(self) -> str:
        """Return string representation."""
        msg = (
            f"Cannot select strategy because of conflicting parameters "
            f"radius = [{self.radius}] "
            f"destinations = [{self.destinations}]"
        )
        return msg

    def __str__(self) -> str:
        """Return string representation."""
        return self.__repr__()

"""Handles query creation and execution for the gmaps service."""

import logging
from typing import List
from google.cloud import bigquery
import googlemaps
from ministorehub_be.requests import RequestParser, ResponseGenerator
from ministorehub_be.data import Coords, CoordsPair
import ministorehub_be.destinations_builder

logger = logging.getLogger(__name__)


class GMapsDMController():
    """Builds a request object, executes it and stores result.

    Build a request object according to the RequestParser's configuration,
    fetching all coordinates in a bounding box of a certain radius, along with
    metadata for each coordinate.

    Uses this request object to query GMaps distance matrix and stores the
    results within the request object.

    Attributes:
        map_data: stores fetched coordinates, metadata and travel information
        bq_client: BigQuery client
        reqp: RequestParser
        gmaps_client: GMaps client
    """

    def __init__(self):
        """Initialize object."""
        self.map_data: List[CoordsPair] = []
        self.bq_client = bigquery.Client()
        self.reqp: RequestParser = None
        self.gmaps_client = None

    def set_request_parser(self, reqp: RequestParser):
        """Set a request parser to configure the class."""
        self.reqp = reqp
        api_key = self.reqp.get_gmaps_api_key()
        self.gmaps_client = googlemaps.Client(api_key)

    def configure_request(self):
        """Configure request.

        Get a list of coordinates inside a box around the origin with sides of
        size radius. Put the information into CoordsPair for easy querying of
        GMaps distance matrix.
        """
        self.map_data = []

        dest_builder = ministorehub_be.destinations_builder.create(self.reqp)

        query = dest_builder.build_query()

        logger.debug("Destinations query: %s", query)

        result = self.bq_client.query(query).result()

        logger.debug("Destinations query result %s", result)

        for row in result:
            items = list(row.items())

            uid: str = None
            lat: str = None
            lon: str = None
            extra_data: dict[str, str] = {}

            for column, data in items:
                if column == self.reqp.get_id_col():
                    uid = data
                elif column == self.reqp.get_lat_col():
                    lat = data
                elif column == self.reqp.get_lon_col():
                    lon = data
                else:
                    extra_data[column] = data

            current_coords = Coords(lat, lon, uid)
            current_coords.set_extra_data(extra_data)
            pair = CoordsPair(self.reqp.get_origin(), current_coords)
            pair.set_mode(self.reqp.get_mode())
            self.map_data.append(pair)

        logger.info("Fetched %d destinations", len(self.map_data))
        logger.debug("Destinations %s", self.map_data)

    def make_request(self):
        """Make a request with the current settings."""
        for pair in self.map_data:
            matrix = self.gmaps_client.distance_matrix(pair.get_pt1(),
                                                       pair.get_pt2(),
                                                       mode=pair.mode)
            logger.debug("Matrix: %s", matrix)
            data = matrix['rows'][0]['elements'][0]
            pair.set_distance(data['distance']['value'])
            pair.set_time(data['duration']['value'])

        logger.debug("Request finished: %s", self.map_data)

    def to_json(self) -> str:
        """Return json representation."""
        response = ResponseGenerator()
        response.set_status("ok")
        response.set_msg("ok")
        response.set_data_size(len(self.map_data))
        response.set_data(self.map_data)

        return response.to_json()

"""Handles logging for the whole application."""
import logging
import sys
import os


class LessThanFilter(logging.Filter):
    """Filters messages that are above certain logging level."""

    def __init__(self, exclusive_maximum):
        """Set maximum level of logging.

        Sets maximum level of logging for a specific stream in order to be able
        to selectively send messages to stdout and stderr.

        Args:
            exclusive_maximum: log level below this will be logged
        """
        super().__init__()
        self.max_level = exclusive_maximum

    def filter(self, record):
        """Filter messages.

        Filter messages above certain logging level.

        Args:
            record: log to parse
        """
        if record.levelno < self.max_level:
            return 1
        return 0


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

LOG_MSG_FORMAT = (
    "%(asctime)s %(levelname)8s "
    "[%(module)s]: %(message)s"
    )
formatter = logging.Formatter(LOG_MSG_FORMAT)

console_stdout_handler = logging.StreamHandler(sys.stdout)
console_stdout_handler.setLevel(logging.DEBUG)
console_stdout_handler.addFilter(LessThanFilter(logging.WARNING))
console_stdout_handler.setFormatter(formatter)
logger.addHandler(console_stdout_handler)

console_stderr_handler = logging.StreamHandler(sys.stderr)
console_stderr_handler.setLevel(logging.WARNING)
console_stderr_handler.setFormatter(formatter)
logger.addHandler(console_stderr_handler)

if "MINISTOREHUB_LOG" not in os.environ:
    logging.disable()

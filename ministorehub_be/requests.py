"""Handles data related to request objects."""
import os
import logging
import json
from typing import List, Any
from ministorehub_be.exceptions import ParameterMissing
from ministorehub_be.data import Coords

logger = logging.getLogger(__name__)


class RequestParser():
    """Handles the request object.

    Provides access to request data across the whole application.

    Attributes:
        arguments: JSON representation of POST data
    """

    def __init__(self, request):
        """Initialize object.

        Args:
            request: request received by the webserver
        """
        self.arguments = request.json
        logger.debug("Request data: %s", self.arguments)

    def get_origin(self) -> Coords:
        """Return origin coordinates.

        Transform the string version of the coordinates (e.g. "17.5,13.4") into
        a Coords object.
        """
        origin_raw = self.get_request_param('origin')
        origin_list = origin_raw.split(',')
        origin = Coords(origin_list[0], origin_list[1], "origin")

        return origin

    def get_radius(self) -> int:
        """Return radius in meters."""
        return self.get_request_param('radius', '')

    def get_id_col(self) -> str:
        """Return name of id column."""
        return self.get_env_param('MINISTOREHUB_IDCOL')

    def get_table(self) -> str:
        """Return name of table to query."""
        return self.get_env_param('MINISTOREHUB_TABLE')

    def get_lat_col(self) -> str:
        """Return name of latitude column."""
        return self.get_env_param('MINISTOREHUB_LATCOL')

    def get_lon_col(self) -> str:
        """Return name of longitude column."""
        return self.get_env_param('MINISTOREHUB_LNGCOL')

    def get_mode(self) -> str:
        """Return navigation mode."""
        return self.get_request_param('navigation', 'driving')

    def get_gmaps_api_key(self) -> str:
        """Return gmaps api key."""
        return self.get_env_param('GMAPS_API_KEY')

    def get_extra_cols(self) -> str:
        """Return extra columns to select in query."""
        return self.get_env_param('MINISTOREHUB_EXTRA_COLS')

    def get_destinations(self) -> str:
        """Return destination list.

        Need to split the comma-delimited string into a list of items, the
        quote each item individually and assemble them together again as a
        comma-delimited string.

        NB: if destinations is empty, return an empty string, passing an empty
        string through split() makes python think that there's a string
        composed of two single quotes and that messes up the logic everywhere.
        """
        dest = self.get_request_param('destinations', '')
        if not dest:
            return ''
        return ','.join(f"'{i}'" for i in dest.split(','))

    def get_env_param(self, param):
        """Return environment variable.

        Args:
            param: the variable to fetch

        Raises:
            ParameterMissing: a mandatory parameter is missing
        """
        try:
            return os.environ[param]
        except KeyError as key_not_found:
            raise ParameterMissing('env', param) from key_not_found

    def get_request_param(self, param, default=None):
        """Return parameter from URL.

        Scan the URL for a given parameter

        Args:
            param: what we're looking for
            default: return this when the parameter can't be found

        Raises:
            ParameterMissing: a mandatory parameter is missing
        """
        if param in self.arguments:
            return self.arguments[param]
        if default is not None:
            return default
        raise ParameterMissing('URL', param)


class ResponseGenerator():
    """Generates responses for the request.

    Generates a JSON response for the web request.

    Attributes:
        status: text representing status of the request
        msg: text representing any additional information related to the status
        of the request
        data_size: amount of data items returned
        data: list of data items
    """

    def __init__(self):
        """Initialize object."""
        self.status: str = None
        self.msg: str = None
        self.data_size: int = None
        self.data: List = []

    def set_status(self, status: str):
        """Set status of response."""
        self.status = status

    def set_msg(self, msg: str):
        """Set msg of response."""
        self.msg = msg

    def set_data_size(self, size: int):
        """Set amount of data to return."""
        self.data_size = size

    def set_data(self, data: List):
        """Set data of the response."""
        self.data = data

    def to_json(self) -> str:
        """Return JSON representation of a response.

        something
        """
        response: dict[str, Any] = {}
        response['status'] = self.status
        response['msg'] = self.msg
        response['data_size'] = self.data_size
        response['results'] = self.data

        return json.dumps(response, indent=2, default=self.obj_dict)

    def obj_dict(self, obj):
        """Return the object's dictionary.

        This function is used within json.dumps() for any time it doesn't know
        how to JSON serialize an object, this function tells it to just dump
        the dictionary.
        """
        return obj.__dict__
